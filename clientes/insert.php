<?php
require_once('./header.php');
require_once('../classes/crud.php');
$crud = new Crud('clientes',$pdo);

print '<h3 align="center">'.$crud->appName.'</h3>';
print '</div>';
// Mostrar o nome da tabela
print '<h3 align="center">'.ucfirst($crud->table).'</h3>';
?>

<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
        <table class="table table-bordered table-responsive">    
            <form name="frm" method="post" action="insert.php">
				<!-- ************** método inputsInsert() ******************* 
				 categoria_id - campo da tabela atual relacionado com a categorias, que é o $fk
				 categorias - nome da tabela estrangeira, 
				 id - campo id da tabela categorias, 
				 descricao - campo de categorias para exibir na combo
				 Se tabela não for relacionada usar para $fk = 'n' 
				 Para tabelas não relacionadas chamar apenas com print $crud->inputsInsert('n'); -->

				<?php 
											// 		$fk			$tbl		 $id  $desc
					print $crud->inputsInsert('categoria_id','categorias','id','descricao');
					// Tabelas sem relacionamento
					//print $crud->inputsInsert('n');
				?>

            </form>
        </table>
        </div>
    </div>
</div>

<?php
require_once('./footer.php');
require_once('./insert_db.php');
?>
