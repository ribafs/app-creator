<?php
require_once('./header.php');
require_once('../classes/crud.php');
$crud = new Crud('produtos',$pdo);

// Receebr o id via GET do busca_resultados.php ou via POST deste arquivo
if(isset($_GET['id'])){
	$id=$_GET['id'];
}else{
	$id=$_POST['id'];
}

// Mostrar nome da Tabela
print '<h3 align="center">'.ucfirst($crud->table).'</h3>';
?>

<!-- Mostrar form de atualização -->
<div class="container" align="center">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <form name="frm" method="post" action="update.php">
                <table class="table table-bordered table-responsive table-hover">

 				 <?php
				 	 $sth = $crud->pdo->prepare("SELECT * from ".$crud->table." WHERE id = :id");
					 $sth->bindValue(':id', $id, PDO::PARAM_STR); // No select e no delete basta um único bindValue
					 $sth->execute();
					 $reg = $sth->fetch(PDO::FETCH_OBJ);

					 /************** método inputsInsert() ******************* 
					 categoria_id - campo da tabela atual relacionado com a categorias, que é o $fk 
					 categorias - nome da tabela estrangeira $tbl, 
					 id - campo id da tabela categorias $id, 
					 $reg - variável deste arquivo 
					 descricao - campo para exibir na combo, $desc
					 */
											 // $fk			  $reg	 $tbl		   $id	 $desc
					 // print $crud->inputsUpdate('categoria_id', $reg, 'categorias', 'id', 'descricao');

					 // Tabela não relacionada
					 print $crud->inputsUpdate('n', $reg);
				 ?>      
				 </table>
	      	</form>
	   </div>
	</div>
</div>
<?php
require_once('./footer.php');
require_once('./update_db.php');
?>
