Uma dificuldade de trabalhar com javascript/jquery/ajax é a carência de debug.

Em nosso aplicativo a paginação é criada com a ajuda do AJAX e ao efetuar alterações na classe de conexão o aplicativo deixou de mostrar osr egistros e nenhuma mensagem de erro ou aviso.

Nestas horas precisamos usar o inspetor para verificar algo no Console para ver alguma mensagem de erro.

Outra boa alternativa, descoberta por acaso, é, no navegador, teclar Ctrl+U para visualizar o código fonte e então ler com calma pois verá em meio ao código mensagens de erro. Foi assim que consegui debugar o problema que gerei ao alerar uma classe e ter esquecido de ajustar no código ajax em index.php.


